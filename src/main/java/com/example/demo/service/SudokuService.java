package com.example.demo.service;


import com.example.demo.component.CsvReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class SudokuService {

    private final CsvReader csvReader;
    List<Integer> rowIds = new ArrayList<>();
    List<Integer> columnIds = new ArrayList<>();
    List<Integer> areaIds = new ArrayList<>();
    @Autowired
    public SudokuService(CsvReader csvReader){
        this.csvReader=csvReader;
    }

    public boolean verify(){
        rowIds.clear();
        columnIds.clear();
        areaIds.clear();
        csvReader.readCsvFile();
        Integer[][] board= csvReader.getSudokuArray();
        checkColumns(board);
        checkRows(board);
        checkSquares(board);
        return rowIds.size() == 0 && columnIds.size() == 0 && areaIds.size() == 0;
    }
    private void checkColumns(Integer[][] board){
        for(int i=0;i<9;i++){
            Set<Integer> set = new HashSet<>();
            for(int j=0;j<9;j++){
                if (set.add(board[j][i]) == false) {
                    columnIds.add(i);
                    break;
                }
            }
        }
    }
    private void checkRows(Integer[][] board){

        for(int i=0;i<9;i++){
            Set<Integer> set = new HashSet<>();
            for(int j=0;j<9;j++){
                if (set.add(board[i][j]) == false) {
                    rowIds.add(i);
                    break;
                }
            }
        }
    }
    private void checkSquares(Integer[][] board){
        int position=0;
        for(int i=0;i<9;i++){
            if(i%3==0 && i!=0){
                position++;
            }
            Set<Integer> set = new HashSet<>();

            outerLoop:
            for(int j = position * 3; j<3+position*3; j++){
               for(int k = i % 3 * 3; k<3+i%3*3; k++){
                   if (set.add(board[j][k]) == false) {
                       areaIds.add(i);
                       break outerLoop;
                   }
               }
            }

        }
    }

    public Map<String, List<Integer>> getErrorMessage() {
        Map<String, List<Integer>> errorMessage = new HashMap<>();
        errorMessage.put("rowIds", rowIds);
        errorMessage.put("columnIds", columnIds);
        errorMessage.put("areaIds", areaIds);
        return errorMessage;
    }
}
