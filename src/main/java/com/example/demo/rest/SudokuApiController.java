package com.example.demo.rest;

import com.example.demo.service.SudokuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class SudokuApiController {
    private final SudokuService sudokuService;

    @Autowired
    public SudokuApiController(SudokuService sudokuService) {
        this.sudokuService = sudokuService;
    }

    @PostMapping("/api/sudoku/verify")
    public ResponseEntity <Map<String, List<Integer>>> verify(){
        if(sudokuService.verify()) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(sudokuService.getErrorMessage(),HttpStatus.BAD_REQUEST);
        }
    }


}
