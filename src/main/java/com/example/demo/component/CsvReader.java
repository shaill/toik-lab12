package com.example.demo.component;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class CsvReader {

    private Integer[][] sudokuArray;
    String PATH;

    public CsvReader() {
        PATH = "src/main/resources/sudoku.csv";
        sudokuArray = new Integer[9][9];
    }

    public void readCsvFile() {

        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {
            String line;
            int i=0;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(",");
                for (int j= 0; j < 9; j++) {
                    System.out.println(values[j]);
                    sudokuArray[i][j] = Integer.parseInt(values[j]);
                }
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Integer[][] getSudokuArray(){
        return sudokuArray;
    }
}
